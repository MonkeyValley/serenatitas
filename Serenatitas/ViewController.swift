//
//  ViewController.swift
//  Serenatitas
//
//  Created by TritonSoft on 02/05/21.
//

import UIKit
import Lottie


class ViewController: UIViewController {

    
    @IBOutlet weak var animationViewStoriboard:AnimationView!
    private var animationView: AnimationView?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        animationView = .init(name: "singin")
        animationView!.frame = animationViewStoriboard.bounds
        animationView!.frame.size.width = animationViewStoriboard.frame.width - 40
        animationView!.contentMode = .scaleAspectFill
        animationView!.loopMode = .loop
        animationView!.animationSpeed = 0.5
        animationViewStoriboard.addSubview(animationView!)
          
        animationView?.play()
    }


}

